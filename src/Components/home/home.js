import { uid } from 'uid';
import './home.css';
import { useSelector } from 'react-redux';
import { useState } from 'react';
import { Redirect } from 'react-router';

const Home = () => {
  const teams = useSelector((state) => state.teams);
  const [redirectToAddTeam, setRedirectToAddTeam] = useState(false);

  const renderTeams = () => {
    const team_renders = [];
    teams.forEach((team) =>
      team_renders.push(
        <div key={uid()} className='card card-team'>
          <div className='card-body'>
            <img src={team.img} alt={team.name} />
            <p>{team.name}</p>
          </div>
        </div>
      )
    );
    return team_renders;
  };

  const addTeam = () => {
    setRedirectToAddTeam(true);
  };

  if (redirectToAddTeam) {
    return <Redirect push to='/add-team' />;
  }

  return (
    <div className='home-page container'>
      <h1 className='text-center home-title'>Welcome to the Home Page</h1>
      <button
        onClick={addTeam}
        type='button'
        className='btn btn-block btn-primary'
      >
        Add Team
      </button>
      <div className='team-view'>{renderTeams()}</div>
    </div>
  );
};

export default Home;
