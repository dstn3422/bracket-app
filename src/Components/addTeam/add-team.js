import { useDispatch, useSelector } from 'react-redux';
import './add-team.css';
import { ADD_TEAM } from '../../Constants';
import { useState } from 'react';
import Team from '../../Models/Team';
import { Redirect } from 'react-router';

const AddTeam = () => {
  const dispatch = useDispatch();

  const [team, setTeam] = useState(new Team('Name', 'Image Url'));
  const [teamAdded, setTeamAdded] = useState(false);
  const teams = useSelector((state) => state.teams);

  const submitTeam = (e) => {
    e.preventDefault();
    dispatch({ type: ADD_TEAM, team });
    console.log(teams);
    setTeamAdded(true);
  };

  const handleInputChange = (e, type) => {
    const newTeam = new Team(team.name, team.img);
    newTeam[type] = e.target.value;
    setTeam(newTeam);
  };

  let content = (
    <div className='add-team container'>
      <h1 className='text-center'>Add a Team</h1>
      <form>
        <label htmlFor='name'>Name:</label>
        <input
          onChange={(e) => handleInputChange(e, 'name')}
          value={team.name}
          name='name'
          type='text'
          className='form-control'
        />
        <label htmlFor='image'>Image Url:</label>
        <input
          onChange={(e) => handleInputChange(e, 'img')}
          value={team.img}
          name='image'
          type='text'
          className='form-control'
        />
        <button onClick={submitTeam} type='submit' className='btn btn-success'>
          Submit
        </button>
      </form>
    </div>
  );

  if (teamAdded) {
    content = <Redirect push to='/' />;
  }

  return content;
};

export default AddTeam;
