import './App.css';

import Home from './Components/home/home';
import AddTeam from './Components/addTeam/add-team';

import { BrowserRouter as Router, Route } from 'react-router-dom';

const App = () => {
  return (
    <div className='App'>
      <Router>
        <Route exact path='/' component={Home} />
        <Route path='/add-team' component={AddTeam} />
      </Router>
    </div>
  );
};

export default App;
